import React from "react";
import { Panel } from "./Panel";

interface Props {
    panel: Panel,
    coordinates: number[],
    fill: string,
    opacity: string,
    border: number,
    triangle: boolean
}

const squarePoints = (x: number, y: number): number[][] => {
    return [[x, y], [x + 1, y], [x + 1, y + 1], [x, y + 1]];
}

const h = Math.sqrt(3) / 2.0;

const trianglePoints = (x: number, y: number): number[][] => {
    return ( (x + y) % 2)
        ? [[x / 2, y * h], [(x - 1) / 2, (y + 1) * h], [(x + 1) / 2, (y + 1) * h]]
        : [[(x - 1) / 2, y * h], [(x + 1) / 2, y * h], [x / 2, (y + 1) * h]];
}

export const Polygon: React.FC<Props> = ({ panel, coordinates, fill, opacity, border = 0, triangle = false }: Props) => {

    const mapper = ((ax: number) => (c: number) => panel.center()[ax] + (c * panel.blocksize));
    const tilePoints = triangle ? trianglePoints(coordinates[0], coordinates[1]) : squarePoints(coordinates[0], coordinates[1]);
    const mappedPoints = tilePoints.map((p) => [mapper(0)(p[0]), mapper(1)(p[1])]);

    const points = mappedPoints.reduce(((result, coors) => result + " " + coors[0] + "," + coors[1]), "");

    return (<polygon points={points} fill={fill} opacity={opacity} strokeWidth={border} stroke="black" />);
}

