import * as React from 'react';
import { Panel } from './Panel';
import { Polygon } from "./Polygon"
import { Border } from "./Border"
import { ColorPickerComponent } from "./ColorPickerComponent"
import { ColorChangeHandler, ColorResult, RGBColor } from 'react-color';
import { Position, Tile, Pattern, PatternElement, Element } from './Pattern';
import { FileDialogueButton } from './FileDialogue';
import { FilePersister, LoadedDrawing } from './FilePersister';
import { Transformer } from './Transformer';
import { SvgExporter } from './SvgExporter';


const App: React.FC = () => {

  const panel: Panel = new Panel([10, 10], [800, 800], 10);

  const [color, setColor] = React.useState<RGBColor>({ b: 0, g: 0, r: 0 } as RGBColor);
  const [update, setUpdate] = React.useState(true);
  const [cursor, setCursor] = React.useState([0, 0] as Position);
  const [drawing, setDrawing] = React.useState<PatternElement[]>([]);
  const [patterns, setPatterns] = React.useState<Pattern[]>([]);
  const [currentPatternIdx, setCurretPatternIdx] = React.useState(-1);
  const [selectedTile, setSelectedTile] = React.useState<Tile | null>(null);
  const [triangles, setTriangles] = React.useState(true);


  const selected = (): Element => {
    if (currentPatternIdx === -1) {
      if (selectedTile !== null) {
        return selectedTile;
      }
      const tile = new Tile(color);
      setSelectedTile(tile);
      return tile;
    }
    return patterns[currentPatternIdx];
  };


  const colorPickHandler: ColorChangeHandler = (color: ColorResult, event) => {
    if (!selectedTile || selected() instanceof Tile) {
      setSelectedTile(new Tile(color.rgb));
    } else {
      selectedTile.color = color.rgb;
      setSelectedTile(selectedTile);
    }
    setColor(color.rgb);
  }

  const fileSelectedHandler = (fileList: FileList) => {
    if (fileList.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', (event: any) => {
        const ld: LoadedDrawing = FilePersister.load(event.target.result);
        setPatterns(ld.patterns);
        setDrawing(ld.drawing);
        setTriangles(ld.triangles);
      });
      reader.readAsText(fileList[0]);
    }

  }

  const keyPressHandler = ({ key }: { key: string }) => {

    switch (key) {

      case "ArrowUp":
        cursor[1]--;
        setCursor(cursor);
        break;
      case "ArrowDown":
        cursor[1]++;
        setCursor(cursor);
        break;
      case "ArrowLeft":
        cursor[0]--;
        setCursor(cursor);
        break;
      case "ArrowRight":
        cursor[0]++;
        setCursor(cursor);
        break;

      case " ": // Space = Add selected pattern on cursor position
        const cursorPosition = [...cursor];
        const newElement = new PatternElement(cursorPosition, selected());
        drawing.push(newElement);
        setDrawing(drawing);
        break;

      case "c": // c = Create new pattern
        if (drawing) {
          const newPattern = new Pattern(drawing.map(c => Transformer.shiftElement(c, [-cursor[0], -cursor[1]])));
          patterns.push(newPattern);
          setPatterns(patterns);
          setDrawing([]);
        }
        break;

      case "e": // e = Export to SVG
        SvgExporter.export(triangles, drawing, patterns);
        break;

      case "g": // switch grid
        setTriangles(!triangles);
        break;

      case "p":
        var nextPatternIdx = -1;
        if (currentPatternIdx < patterns.length - 1) {
          nextPatternIdx = currentPatternIdx + 1;
        }
        setCurretPatternIdx(nextPatternIdx);
        setSelectedTile(null);
        break;

      case "r": // r = Replace current pattern
        if (drawing && currentPatternIdx >= 0) {
          const newPattern = new Pattern(drawing.map(c => Transformer.shiftElement(c, [-cursor[0], -cursor[1]])));
          patterns[currentPatternIdx] = newPattern;
          setPatterns(patterns);
          setDrawing([]);
        }
        break;

      case "s":
        FilePersister.save(triangles, drawing, patterns);
        break;

      case "t":
        const s = selected();
        if (s instanceof Pattern) {
          const tiles = Array.from(new Set(s.elements.map(pe => pe.element()).filter(e => e instanceof Tile) as Tile[]));
          const idx = tiles.findIndex(t => t === selectedTile) + 1;
          if (idx < tiles.length) {
            setSelectedTile(tiles[idx]);
          } else {
            setSelectedTile(null);
          }
        }
        break;

      case "u":
        if (drawing.length > 0) {
          setDrawing(drawing.slice(0, drawing.length - 1))
        }
        break;

      default:
        break;
    }

    setUpdate(!update);
  }

  React.useEffect(() => {
    window.addEventListener('keydown', keyPressHandler);
    return () => {
      window.removeEventListener('keydown', keyPressHandler);
    };
  });

  return (
    <div className="App" >
      <svg key="drawing" width={panel.topLeft[0] + panel.area[0]} height={panel.topLeft[1] + panel.area[1]}>
        <Border key="border" panel={panel} />
        {Transformer.flatten(drawing, [0, 0] as Position, (el: Element) => (el === selectedTile)).map(gridTile =>
          (<Polygon key={gridTile.id} panel={panel} coordinates={gridTile.position} triangle={triangles} fill={gridTile.rgb()} opacity={"1.0"} border={gridTile.selectedTile ? 1 : 0} />))}
        {Transformer.flattenElement(selected(), cursor, (el: Element) => (el === selectedTile)).map(gridTile =>
          (<Polygon key={gridTile.id} panel={panel} coordinates={gridTile.position} triangle={triangles} fill={gridTile.rgb()} opacity={"0.5"} border={gridTile.selectedTile ? 1 : 0} />))}
      </svg>

      <ColorPickerComponent key="colorPicker" color={color} onPickColor={colorPickHandler} />
      <FileDialogueButton key="fileDialogButton" buttonText='Load drawing' multiple={false} onChangeHandler={fileSelectedHandler} />

    </div>
  );
}

export default App;






