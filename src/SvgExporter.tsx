import { saveAs } from 'file-saver';
import { Panel } from './Panel';
import { Position, Pattern, PatternElement, Element } from './Pattern';
import { Polygon } from './Polygon';
import { Transformer } from './Transformer';
import { renderToString } from 'react-dom/server'


export class SvgExporter {

  static export(triangles: boolean, drawing: PatternElement[], patterns: Pattern[]) {

    const panel = new Panel([10, 10], [800, 800], 10);

    const polygons = Transformer.flatten(drawing, [0, 0] as Position, (el: Element) => false)
      .map(gridTile => <Polygon panel={panel} coordinates={gridTile.position} triangle={triangles} fill={gridTile.rgb()} opacity={"1.0"} border={0} />)
      .map(reactElement => renderToString(reactElement))
      .reduce((result, line) => result + line + "\n", "")

    const content = '' + 
    '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n' +
    '<!-- Created with http://math.vanhaastert.nl) -->\n\n' +
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 800">\n' + 
    '<g>\n' +
    polygons + 
    '</g>\n' +
    '</svg>\n';

    var blob = new Blob([content], { type: "image/svg+xml;charset=utf-8" });
    saveAs(blob, "tesselation.svg");
  }

}
