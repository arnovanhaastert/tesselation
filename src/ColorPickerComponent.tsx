import React, { CSSProperties } from 'react'
import { RGBColor, ColorChangeHandler, SketchPicker } from 'react-color'
import reactCSS from 'reactcss'

interface Props {
  color: RGBColor,
  onPickColor: ColorChangeHandler;
}

export const ColorPickerComponent: React.FC<Props> = ({ color, onPickColor }: Props) => {

  const [state, setState] = React.useState({ displayClrPkr: false });


  const onClick = () => {
    setState({
      displayClrPkr: !state.displayClrPkr
    })
  };

  const stateClose = () => {
    setState({
      displayClrPkr: false
    })
  };


  const styles = reactCSS({
    'default': {
      color: {
        width: '50px',
        height: '20px',
        borderRadius: '4px',
        background: `rgb(${color.r}, ${color.g}, ${color.b})`,
      },
      popover: {
        position: 'absolute',
        zIndex: '2',
      },
      cover: {
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px',
        position: 'fixed',
      },
      swatch: {
        padding: '5px',
        background: '#ffffff',
        borderRadius: '3px',
        cursor: 'pointer',
        display: 'inline-block',
        boxShadow: '0 0 0 2px rgba(0,0,0,.3)',
      },
    },
  });

  return (
    <div>
      <div style={styles.swatch} onClick={onClick}>
        <div style={styles.color} />
      </div>
      {state.displayClrPkr
        ?
        <div style={styles.popover as CSSProperties}>
          <div style={styles.cover as CSSProperties} onClick={stateClose} />
          <SketchPicker color={color} onChange={onPickColor} />
        </div>
        : null
      }
    </div>
  )
}