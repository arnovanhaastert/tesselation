import { GridTile, Pattern, PatternElement, Position, Tile, Element } from "./Pattern";

export class Transformer {

  static shiftElement = (patternElement: PatternElement, shift: Position) => {
    return new PatternElement(Transformer.shiftPosition(patternElement.position, shift), patternElement.element());
  };

  static shiftPosition = (position: Position, shift: Position) => {
    return [position[0] + shift[0], position[1] + shift[1]];
  };

  static flatten = (elements: PatternElement[], position: Position, isSelected: (el:Element) => boolean) => {
    const result: GridTile[] = [];
    Transformer.flattenInternal(result, elements, position, isSelected);
    return result;
  };

  static flattenElement = (element: Element, position: Position, isSelected: (el:Element) => boolean) => {
    let result: GridTile[] = []
    if (element instanceof Pattern) {
      result = Transformer.flatten(element.elements, position, isSelected);
    }
    else if (element instanceof Tile) {
      result = [new GridTile(position, element.color, isSelected(element)) ];
    }
    return result;
  };


  private static flattenInternal = (result: GridTile[], patternElements: PatternElement[], shift: Position, isSelected: (el:Element) => boolean) => {
    patternElements.forEach(patternElement => {
      const element = patternElement.element();
      const position = Transformer.shiftPosition(patternElement.position, shift);
      if (element instanceof Pattern) {
        Transformer.flattenInternal(result, element.elements, position, isSelected );
      } else if (element instanceof Tile) {
        result.push(new GridTile(position, element.color, isSelected(element)));
      }
    });
    return result;
  };

}
