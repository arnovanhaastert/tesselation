export class Panel {
  topLeft: number[];
  area: number[];
  blocksize: number;

  constructor(topLeft: number[], area: number[], blocksize: number) {
    this.topLeft = topLeft;
    this.area = area;
    this.blocksize = blocksize;
  }

  center: () => number[] = () => {
    return [this.topLeft[0] + this.area[0] / 2, this.topLeft[1] + this.area[1] / 2];
  };
}
