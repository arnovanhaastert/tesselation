import React from "react";
import { Panel } from './Panel';

interface Props {
    panel: Panel;
}

export const Border: React.FC<Props> = ({ panel }: Props) => {
    return (
        <g stroke="black" strokeWidth="2">
            <line x1={panel.topLeft[0]} y1={panel.topLeft[1]} x2={panel.topLeft[0] + panel.area[0]} y2={panel.topLeft[1]} />,
            <line x1={panel.topLeft[0] + panel.area[0]} y1={panel.topLeft[1]} x2={panel.topLeft[0] + panel.area[0]} y2={panel.topLeft[1] + panel.area[1]} />,
            <line x1={panel.topLeft[0] + panel.area[0]} y1={panel.topLeft[1] + panel.area[1]} x2={panel.topLeft[0]} y2={panel.topLeft[1] + panel.area[1]} />,
            <line x1={panel.topLeft[0]} y1={panel.topLeft[1] + panel.area[1]} x2={panel.topLeft[0]} y2={panel.topLeft[1]} />
        </g>

    );
}
