import { saveAs } from 'file-saver';
import { Position, Tile, Pattern, PatternElement, Element } from './Pattern';


export interface LoadedDrawing {
  triangles: boolean;
  version: number;
  tiles: Tile[];
  patterns: Pattern[];
  drawing: PatternElement[];
}

export class FilePersister {

  static save(triangles: boolean, drawing: PatternElement[], patterns: Pattern[]) {
    const tiles: Tile[] = [];
    patterns.forEach(pattern => this.getTiles(tiles, pattern.elements));
    this.getTiles(tiles, drawing);

    const saved = {
      "version": 1,
      "triangles": triangles,
      "tiles": tiles,
      "patterns": patterns,
      "drawing": drawing
    };

    const json = JSON.stringify(saved);
    var blob = new Blob([json], { type: "application/json;charset=utf-8" });
    saveAs(blob, "drawing.tes");
  }

  static load(json: string): LoadedDrawing {
    const saved: any = JSON.parse(json);
    if (saved.version === 1) {
      Element.reset();

      // Load tiles
      const tiles: Tile[] = [];
      saved.tiles.forEach((t: any) => {
        if (!Element.byId.has(t.id)) {
          const tile = new Tile(t.color, t.id);
          tiles.push(tile);
        }
      });


      // Load patterns
      const patterns: Pattern[] = [];
      saved.patterns.forEach((p: any) => {
        if (!Element.byId.has(p.id)) {
          const elements: PatternElement[] = [];
          p.elements.forEach((pe: any) => {
            elements.push(new PatternElement(pe.position as Position, Element.fromId(pe.elementId)));
          });
          const pattern = new Pattern(elements, p.id);
          patterns.push(pattern);
        }
      });

      // Load drawing
      const drawing: PatternElement[] = [];
      saved.drawing.forEach((pe: any) => {
        drawing.push(new PatternElement(pe.position as Position, Element.fromId(pe.elementId)));
      });

      return {
        version: saved.version,
        triangles: !!saved.triangles, 
        tiles: tiles,
        patterns: patterns,
        drawing: drawing
      };

    } else {
      throw new Error("Unexpected file content");
    }
  }

  private static getTiles(tiles: Tile[], elms: PatternElement[]) {
    elms.forEach(patternElement => {
      const element = patternElement.element();
      if (element instanceof Tile) {
        tiles.push(element);
      } else if (element instanceof Pattern) {
        this.getTiles(tiles, element.elements);
      }
    });
  }
}
