import React from "react";

interface Props {
  buttonText: string;
  multiple: boolean;
  onChangeHandler: (fileList: FileList) => void;
}

export const FileDialogueButton: React.FC<Props> = ({ buttonText = "Select files", multiple = false, onChangeHandler }) => {

  function buildFileSelector() {
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    if (multiple) {
      fileSelector.setAttribute('multiple', 'multiple');
    }
    return fileSelector;
  }

  const fileSelector = buildFileSelector();


  const handleFileSelect = (e: any) => {
    e.preventDefault();
    fileSelector.click();
    fileSelector.addEventListener('change', (event: any) => {
      onChangeHandler(event.target.files);
    });
  }

  return <a className="button" href="." onClick={handleFileSelect}>{buttonText}</a>
}

