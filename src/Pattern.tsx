import { RGBColor } from 'react-color';


export type Position = number[];


export class Element {
    static nextId: number = 0;
    static byId: Map<number, Element> = new Map<number,Element>();
    static reset() : void { Element.nextId = 0; Element.byId = new Map<number,Element>(); }
    static fromId(id:number) : Element {
        if (!Element.byId.has(id)) {
            throw new Error(`No element found for id ${id}`);
        }
        return Element.byId.get(id)!!;
    }

    id: number;

    constructor(id?: number) {
        if (id) {
            if (Element.byId.has(id)) {
                throw new Error("Duplicate ids");
            }
            if (id >= Element.nextId) {
                Element.nextId = id + 1;
            }
        } else {
            id = Element.nextId;
            Element.nextId = Element.nextId + 1;
        }
        this.id = id;
        Element.byId.set(this.id, this);
    }

}


export class Pattern extends Element {
    elements: PatternElement[];

    constructor(elements: PatternElement[] = [], id?: number) {
        super(id);
        this.elements = elements;
    }
}

export class Tile extends Element {
    color: RGBColor;

    constructor(color: RGBColor, id?: number) {
        super(id);
        this.color = color;
    }
};


export class GridTile {
    static nextId: number = 0;

    id: string;
    position: Position;
    color: RGBColor;
    selectedTile = false;

    constructor(position: Position, color: RGBColor, selected: boolean) {
        this.id = GridTile.nextId.toString();
        GridTile.nextId++;
        this.position = position;
        this.color = color;
        this.selectedTile = selected;
    };

    rgb(): string {
        return `rgb(${this.color.r},${this.color.g},${this.color.b})`;
    };
}



export class PatternElement {
    position: Position;
    elementId: number;

    constructor(position: Position, element: Element) {
        this.position = position;
        this.elementId = element.id;
    };

    element(): Element {
        return Element.byId.get(this.elementId) as Element;
    };
}
